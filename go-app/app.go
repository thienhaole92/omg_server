package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
)

type AppCollections struct {
	todos *mongo.Collection
}

type App struct {
	mongo       *mongo.Client
	collections *AppCollections
	router      *mux.Router
}

func (app *App) ConnectMongo() {
	fmt.Println("Starting mongo todo app...")

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://neko:123@mongo1:27017,mongo2:27017,mongo3:27017/mydb?replicaSet=rs0")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	app.mongo = client
	fmt.Println("Connected to MongoDB!")
}

func (app *App) SetupCollections() {
	// Setup collections
	app.collections = new(AppCollections)
	app.collections.todos = app.mongo.Database("mydb").Collection("todos")
	fmt.Println("Initialised collections: [todos]")
}

func (app *App) StartServer() {
	app.router = CreateRouter(app.collections)
	fmt.Println("Starting HTTP Server on :4000")
	log.Fatal(http.ListenAndServe(":4000", app.router))
}
