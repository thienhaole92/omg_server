package main

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"fmt"
	"net/http"
	"github.com/gorilla/websocket"
)

type longLatStruct struct {
	Long float64 `json:"longitude"`
	Lat  float64 `json:"latitude"`
}

var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan *longLatStruct)
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true	
	},
}

func CreateRouter(collections *AppCollections) *mux.Router {
	r := mux.NewRouter()

	// health check endpoint
	r.HandleFunc("/status", HealthCheckHandler)

	// todos CRUD
	r.Methods("GET").Path("/todos").HandlerFunc(FetchTodosHandler(collections))
	r.Methods("POST").Path("/todos").HandlerFunc(AddTodoHandler(collections))
	r.Methods("PATCH").Path("/todos").HandlerFunc(UpdateTodoHandler(collections))
	r.Methods("DELETE").Path("/todos/{id}").HandlerFunc(DeleteTodoHandler(collections))
	r.HandleFunc("/longlat", longLatHandler).Methods("POST")
	r.HandleFunc("/ws", wsHandler)
	go echo()

	return r
}

func writer(coord *longLatStruct) {
	broadcast <- coord
}


func longLatHandler(w http.ResponseWriter, r *http.Request) {
	var coordinates longLatStruct
	if err := json.NewDecoder(r.Body).Decode(&coordinates); err != nil {
		log.Printf("ERROR: %s", err)
		http.Error(w, "Bad request", http.StatusTeapot)
		return
	}
	defer r.Body.Close()
	go writer(&coordinates)
}


func wsHandler(w http.ResponseWriter, r *http.Request) {
        ws, err := upgrader.Upgrade(w, r, nil)
        if err != nil {
                log.Fatal(err)
        }

        // register client
        clients[ws] = true	
}

// 3
func echo() {
        for {
                val := <-broadcast
                latlong := fmt.Sprintf("%f %f %s", val.Lat, val.Long)
                // send to every client that is currently connected
                for client := range clients {
                        err := client.WriteMessage(websocket.TextMessage, []byte(latlong))
                        if err != nil {
                                log.Printf("Websocket error: %s", err)
                                client.Close()
                                delete(clients, client)
                        }
                }
        }
}

func HealthCheckHandler(w http.ResponseWriter, _ *http.Request) {
	if err := json.NewEncoder(w).Encode(map[string]bool{"ok": true}); err != nil {
		log.Printf("Error responding to health check: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

type FetchTodosResponse struct {
	Items []*Todo `json:"items"`
}

func FetchTodosHandler(collections *AppCollections) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		var cursor *mongo.Cursor

		filter := bson.D{}
		opts := &options.FindOptions{}

		cursor, err = collections.todos.Find(context.TODO(), filter, opts)
		if err != nil {
			log.Printf("Find error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		CloseCursor := func() {
			if err := cursor.Close(context.TODO()); err != nil {
				log.Printf("Close cursor error: %s", err)
			}
		}

		defer CloseCursor()

		var items []*Todo
		for cursor.Next(context.TODO()) {
			var item *Todo

			err = cursor.Decode(&item)
			if err != nil {
				log.Printf("Decode BSON error: %s", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			items = append(items, item)
		}

		err = json.NewEncoder(w).Encode(FetchTodosResponse{items})
		if err != nil {
			log.Printf("Encode JSON response error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

type AddTodoPayload struct {
	Title string `json:"title"`
}

type AddTodoResponse struct {
	ID string `json:"id"`
}

func AddTodoHandler(collections *AppCollections) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		var payload AddTodoPayload
		var writeResult *mongo.InsertOneResult

		err = json.NewDecoder(r.Body).Decode(&payload)
		if err != nil {
			log.Printf("Error decoding body: %s", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		writeResult, err = collections.todos.InsertOne(context.TODO(), payload)
		if err != nil {
			log.Printf("Insert todo error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		id := writeResult.InsertedID.(primitive.ObjectID).Hex()
		err = json.NewEncoder(w).Encode(AddTodoResponse{id})
		if err != nil {
			log.Printf("Encode JSON response error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func UpdateTodoHandler(collections *AppCollections) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {}
}

type DeleteTodoPayload struct {
	ID string `json:"id"`
}

type DeleteTodoResponse struct {
	ID string `json:"id"`
}

func DeleteTodoHandler(collections *AppCollections) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		payload := &DeleteTodoPayload{}
		payload.ID = mux.Vars(r)["id"]
		var deleteResult *mongo.DeleteResult

		filter := map[string]string{"_id": payload.ID}

		deleteResult, err = collections.todos.DeleteOne(context.TODO(), filter)
		if err != nil {
			log.Printf("Error deleting item: %s", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if deleteResult.DeletedCount == 0 {
			err = errors.New("deleted count is 0")
			log.Printf("Error deleting item: %s", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = json.NewEncoder(w).Encode(DeleteTodoResponse{payload.ID})
		if err != nil {
			log.Printf("Encode JSON response error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
