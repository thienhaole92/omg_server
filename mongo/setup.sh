#!/bin/bash
echo "sleeping for 30 seconds"
sleep 30

echo setup.sh time now: `date +"%T" `
mongo --host mongo1:27017 -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD" <<EOF
  var cfg = {
    "_id": "rs0",
    "version": 1,
    "members": [
      { _id : 0, host : "mongo1:27017", priority:1 },
      { _id : 1, host : "mongo2:27017", priority:1 },
      { _id : 2, host : "mongo3:27017", priority:1 }
    ]
  };
  rs.initiate(cfg);
  rs.conf();
EOF

echo "sleeping for 30 seconds"
echo "waiting while ReplicaSet configuration is being applied"
sleep 30
mongo --host mongo1:27017 -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD" <<EOF
  rs.status();
EOF

echo "sleeping for 30 seconds"
sleep 30
mongo --host mongo1:27017 -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD" <<EOF
  var mydb = db.getSiblingDB("$APP_DB")
  mydb.createUser({user: "$APP_USER", pwd: "$APP_PASSWORD", roles: [{role: 'readWrite', db: "$APP_DB"}]});
EOF